import React from 'react';
import logo from './logo.svg';
import './App.css';
import HelloMessage from './components/HelloMessage'
import Clock from './components/clock'
import Timer from './components/timer'
import ClickCount from './components/clickCount'
import TodoApp from './components/toDo'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        
        
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <Clock delay="5000" />
        <Timer delay="1000"/>
        <ClickCount/>
        <Timer delay="3000"/>
        <ClickCount/>
        <Timer delay="4000"/>
        <Clock delay="2000" />
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Aprendamos Juntos
        </a>   
        <TodoApp/>       
      </header>
      
    <HelloMessage name="Taylor" surname="Marcatore" couluer="grey"/>
    <HelloMessage name="Funciono" surname="Mancurso" couleur="blue"/>
    
    </div>
 
  );
}


export default App;

import React from 'react'

class ClickCount extends React.Component {
    constructor(props) {
      super(props);
      this.state = {count: 0};
  
      // This binding is necessary to make `this` work in the callback
      this.handleClick = this.handleClick.bind(this);
    }
  
    handleClick() {
      this.setState(state => ({
        count: state.count + 1
      }));
    }
  
    render() {
      return (
        <button onClick={this.handleClick}> 
          This button has been cliked {this.state.count} times
        </button>
      );
    }
  }

  export default ClickCount;

  
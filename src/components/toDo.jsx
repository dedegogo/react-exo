import React from 'react'


class TodoApp extends React.Component {
    constructor(props) {
      super(props);
      // "this" must be bound (.bind(this)) on all functions called by props or onclick. Otherwise "this" will no be recognized 
      this.state = { items: [], text: '' };
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleDeleteParent = this.handleDeleteParent.bind(this);
      this.clearList = this.clearList.bind(this);
    }
  
    render() {
      return (
        <div>
          <h3>TODO</h3>
          <TodoList items={this.state.items} parentDeleteItem={this.handleDeleteParent} />
          <form onSubmit={this.handleSubmit}>
            <label htmlFor="new-todo" className="mr-15">
              What needs to be done? 
            </label>
            <input
              id="new-todo"
              onChange={this.handleChange}
              value={this.state.text}
            />
            <button>
              Mas Cosas {this.state.items.length + 1}
            </button>
          </form>
          <button onClick={this.clearList}>
                Clear List
            </button>
        </div>
      );
    }
  
    handleDeleteParent(childId) {
      this.setState(state => ({
        items: state.items.filter((item) => item.id !== childId),
        text: ''
    } ))}


    handleChange(e) {
      this.setState({ text: e.target.value });
    }

    clearList(e) {
        e.preventDefault()
        this.setState( {items: []})
    }
    handleSubmit(e) {
      e.preventDefault();
      if (!this.state.text.length) {
        return;
      }
      const newItem = {
        text: this.state.text,
        id: Date.now()
      };
      this.setState(state => ({
        items: state.items.concat(newItem),
        text: ''
      }));
    }
  }
  
  class TodoList extends React.Component {
    /* 
     in order to delete an item, we call a function (deleteItem) that takes item.id, 
     this function in turn calls a prop on the parent class and gives the id as an argument.

     the child component only displays each line, the "items" state belongs to the parent, 
     and only the parent can change it. 
     deleteItem() calls this.props.parentDeleteItem
     parentDeleteItem calls this.handleDeleteParent (line 19)
     handleDeleteParent is the function that is going to create a new array that erases the item that
     corresponds to the item.id that commes with deleteItem.

     in order for handleDeleteParent to invoke "this" it must be bound (line 11 .bind(this))

*/
    deleteItem (id, e) {
      this.props.parentDeleteItem(id)
    }
    render() {
      return (
          <div>
          {this.props.items.map(item => (
            <div key={item.id} className="mr-15" >{item.text}  
            <button className="ml-15" onClick={(e) => this.deleteItem(item.id, e)}>delete</button></div>
          ))}

          </div>
      );
    }
  }
 export default TodoApp;
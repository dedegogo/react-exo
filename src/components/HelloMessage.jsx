import React from 'react'

class HelloMessage extends React.Component {
    render() {
      return (
        <h4 style={{'color': this.props.couleur}}>
          Juan Pa {this.props.name} {this.props.surname}
        </h4>
      );
    }
  }
  

  export default HelloMessage;